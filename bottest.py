import json 
import requests
import time


TOKENSIGNAL = "FREE"
URLSIGNAL = "https://cryptoqualitysignals.com/api/getSignal/?api_key=5C371410D1314&exchange=BINANCE&interval=20" 

with open('signal.txt') as json_file:  
    data2 = json.load(json_file)

def get_url(url):
    response = requests.get(url)
    content = response.content.decode("utf8")
    return content

def get_signal():
    signalcontent = get_url(URLSIGNAL)

    data = json.loads(signalcontent)
    signalcount = data["count"]

    print signalcount

    if signalcount == 0:
        print "no signal"
    else:
        x = 0
        while x<signalcount:
            print x + 1 
            print "Signal:" + data["signals"][x]["currency"] + "" + data["signals"][x]["coin"]
            print "Price:" + data["signals"][x]["buy_end"]
            print "SL:" + data["signals"][x]["target1"]
            print "TP:" + data["signals"][x]["stop_loss"] + "\n" 
            x = x + 1


def main():
    get_signal()


if __name__ == '__main__':
    main()